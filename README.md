
## MineOS Standalone has released!

Hello again, dear friend. Thank you for being with us and supporting our ideas throughout the long development cycle. MineOS has finally reached the release stage: now it is a completely independent operating system with its own lightweight development API. Here's a short features list:

-   Multitasking
-   Double buffered graphical user interface
-   Language packs and software localization
-   Multiple user profiles with password authentication
-   Own EEPROM firmware with boot volume choose/format/rename features and Internet Recovery mode
-   File sharing over the local network via modems
-   Client connections to real FTP servers
-   An internal IDE with syntax highlighting and debugger
-   App Market for publishing programs for every MineOS user
-   Error reporting system with the possibility to send information to developers
-   Animations, wallpapers, screensavers, color schemes and huge customization possibilities
-   Open source system API and detailed illustrated documentations

## How to install?

The easiest way is to use default **pastebin** script. This needs the required componets given. Insert OpenOS floppy disk to computer, insert an Internet Card, turn computer on and type the following to console (or use the middle mouse button or the insert key):

	pastebin run VFHzdPKc

You can paste it to console using middle mouse button or insert key (by default). If for some reason the pastebin.com isn't available to you (for example, it's blacklisted on game server or blocked by Internet provider), use alternative command:

	wget -f https://gitlab.com/QuickMuffin8782-CC/MineOS-Kidz/-/raw/master/Installer/BIOS.lua /tmp/bios.lua && flash -q /tmp/bios.lua && reboot

After a moment, a nice system installer will be shown. You will be prompted to select your preferred language, select and format a boot volume, create a user profile and customize some settings. After that, the system will be successfully installed.

## Reqired Componets

-   Tier 3 Computer Case
-   Tier 3 Graphics Card
-   2 sticks of 3.5 Memory
-   A fresh Tier 3 Hard Drive
-   Internet Card
-   OpenOS Operating System floppy disk
-   Tier 3 Screen
-   Keyboard
-   A fresh EEPROM
