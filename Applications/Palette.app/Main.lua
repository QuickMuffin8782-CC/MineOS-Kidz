
local GUI = require("GUI")
local system = require("System")

local workspace, window = system.addWindow(GUI.palette(1, 1, 0x9900FF))
window.submitButton.text = "Preview color"
window.cancelButton.text = "Close pallette"
window.cancelButton.onTouch = function()
	window:remove()
	workspace:draw()
end
window.submitButton.onTouch = function()
	GUI.alert("Functionallity of the preview color will come soon in a next major update!")
end



window.cancelButton.onTouch = window.submitButton.onTouch
